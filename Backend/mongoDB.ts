import { Db, MongoClient, ObjectId } from "mongodb";

const url = process.env.MONGO_URL || 'mongodb://localhost:27017'
const db = process.env.DBNAME || 'in-demAND';
export const usersCollecton = 'users';
export const skillsCollecton = 'skills';

const client = new MongoClient(url);
let database: Db;

export async function connect(){
    try{
        let mongo_client = await client.connect();
        database = mongo_client.db(db);
    }catch(err){
        console.error(err);
        process.exit();
    }
}

export function getDatabase(){
    return database;
}

export function getObjectId(userId: string){
    try{
        return new ObjectId(userId);
    }catch(err){
        console.error('Error while getting user ObjectID ', '\n', err);
    }
}