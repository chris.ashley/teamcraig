import express from "express";
import * as mongoUtil from "./mongoDB";

let routes = express.Router();

routes.get('/', function (req, res) {
  res.json({"test": "test"})
})

routes.get('/test/get', async function(req,res){

  const collection = mongoUtil.getDatabase().collection(mongoUtil.usersCollecton);

  try{
    let results = await collection.find({}).toArray();
    res.json(JSON.stringify(results))
  }catch(err){
    console.error(err);
  }
  
});

export = routes;