import express from "express";
import * as mongoUtil from "./mongoDB";
import routes from "./routes";
import skills from "./routes/skills";
import register from "./routes/register"
import userRouts from "./routes/user";


//Express Config
const app = express();
const port = process.env.PORT || 8080;

mongoUtil.connect();

app.use(express.json())

app.use(routes);
app.use('/skill', skills)
app.use(register);
app.use('/user', userRouts);

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
