export const feedbackSchema = {
    type: "object",
    properties: {
        name: {type: "string"},
        comment: {type: "string", "maxLength": 255},
        rating: {type: "integer"},
        anonymize: {type: "boolean"}
    },
    anyOf: [
        {required:["name","comment","rating","anonymize"]}
    ] ,
    additionalProperties: false
}

export default feedbackSchema;