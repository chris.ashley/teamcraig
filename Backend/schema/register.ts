export const register_schema = {
    $schema: "http://json-schema.org/draft-07/schema",
    $id: "http://example.com/example.json",
    type: "object",
    title: "The root schema",
    description: "The root schema comprises the entire JSON document.",
    default: {},
    examples: [
        {
            first_name: "Joe",
            last_name: "Bloggs",
            email: "joe.bloggs@and.digital"
        }
    ],
    required: [
        "first_name",
        "last_name",
        "email",
        "available",
        "skills"
    ],
    properties: {
        first_name: {
            $id: "#/properties/first_name",
            type: "string",
            title: "The first_name schema",
            description: "User first name",
            default: "",
            examples: [
                "Joe"
            ]
        },
        last_name: {
            $id: "#/properties/last_name",
            type: "string",
            title: "The last_name schema",
            description: "User last name",
            default: "",
            examples: [
                "Bloggs"
            ]
        },
        email: {
            $id: "#/properties/email",
            type: "string",
            title: "The email schema",
            description: "User email",
            default: "",
            format: "email",
            examples: [
                "joe.bloggs@and.digital"
            ]
        },
        available: {
            $id: "#/properties/available",
            type: "boolean",
            title: "The available schema",
            description: "User availability",
            default: "",
            examples: [
                "true"
            ]
        },
        skills: {
            $id: "#/properties/skills",
            type: "array",
            title: "The skills schema",
            description: "User skills",
            default: ""
        }
    },
    additionalProperties: false
}

export default register_schema;
