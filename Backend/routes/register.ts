import express from "express";
import Ajv from "ajv";
import addFormats from "ajv-formats"
import { getDatabase, usersCollecton } from "../mongoDB";
import schema from "../schema/register"
import * as addr from "email-addresses";

const routes = express.Router();
const ajv = new Ajv();
addFormats(ajv)

const validate = ajv.compile(schema);

routes.post('/register', async (req, res) => {
    const db = getDatabase();

    const valid = validate(req.body);
    if (!valid) {
        res.status(400).send(validate.errors)
        return;
    }

    const { first_name, last_name, email, available, skills } = req.body
    const parsed_email = addr.parseOneAddress(email);

    // type definition for email-addresses package aren't correct, ignore type errors on next line
    // @ts-ignore
    if (parsed_email.domain !== "and.digital") {
        res.status(400).send("User must have an AND.Digital email address")
        return;
    }
    let user = await db.collection(usersCollecton).findOne({ email });

    if (user) {
        res.status(400).send("User with that e-mail address already exists")
        return;
    }

    await db.collection(usersCollecton).insertOne({
        first_name,
        last_name,
        email,
        skills,
        availibility: available,
        average_rating: 0,
        feedback: []

    })
    res.status(201).send("User created");

})

export default routes;
