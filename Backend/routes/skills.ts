import express from "express";
import * as mongoUtil from "../mongoDB";

let routes = express.Router();

routes.get('/getAllSkills', async function(req,res){

  const collection = mongoUtil.getDatabase().collection(mongoUtil.skillsCollecton);

  let results = await collection.find({}).project({_id : 0, name : 1}).sort({name : 1}).toArray();
  res.json(results);

});


routes.get('/skills/:skill', async function(req,res){
  const collection = mongoUtil.getDatabase().collection(mongoUtil.usersCollecton);

  try{
    const skillParam = req.params.skill;
    if (!skillParam) {
      res.status(204).send("Please provide one or   more skills to search for");
      return;
    }

    const skills = skillParam.split(',');
    let query = {};
    query["$or"]=[];
    skills.forEach(skill => {
      query["$or"].push({"skills":{ $regex : skill, $options: 'i'}});
    })

    let results = await collection.find(query).toArray();
    res.json(results);
  }catch(err){
    console.error(err);
  }
  
});

export = routes;