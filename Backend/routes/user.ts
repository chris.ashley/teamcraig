import express from "express";
import Ajv from "ajv";
import addFormats from "ajv-formats";
import feedbackSchema from "../schema/feedback";
import { getDatabase, getObjectId, usersCollecton } from "../mongoDB";

let userRouts = express.Router();

const ajv = new Ajv();
addFormats(ajv);
const validate = ajv.compile(feedbackSchema);

/*
    - This endpoint assumes that the skills will be recieved as a JSON object with an array of skills - {skills: ["Java", "Spring"]}
    - Im not validating it yet as the format we recieve could vary dependent on the front end implementation
    - This endpoint may also never be used if we decide to have a global user update endpoint (its currently in the backlog)
*/
userRouts.post('/:userId/skill/updateSkills', async function(req,res){

    const dbCollection = getDatabase().collection(usersCollecton);
    let objectId = getObjectId(req.params.userId);
    let user = await dbCollection.findOne({_id: objectId});

    if (!user) {
        res.status(404).send("Cannot find user to update skills.");
        return;
    }

    let skills = req.body.skills;

    let result = await dbCollection.updateOne({_id: objectId}, { $set: {skills:skills}});

    if(result.modifiedCount = 1){
        res.status(200).send(`${user.first_name}'s skills have been updated.`);
    }else{
        res.status(500).send(`Something went wrong while trying to update skills. Please try again.`)
    }

});

userRouts.post('/:userId/provideFeedback', async function(req,res){

    let data = req.body;

    const valid = validate(data);

    if (!valid) {
        res.status(400).send(validate.errors);
        return;
    }

    const dbCollection = getDatabase().collection(usersCollecton);

    let objectId = getObjectId(req.params.userId);
    let user = await dbCollection.findOne({_id: objectId});
    
    if (!user) {
        res.status(404).send("Cannot find user to provide feedback");
        return;
    }

    if(data.anonymize) data.name = "Anonymous";

    let result = await dbCollection.updateOne({_id: objectId}, { $push: {"feedback": data}});

    if(result.modifiedCount = 1){
        res.status(200).send(`Feedback added to ${user.first_name}'s profile.`);
    }else{
        res.status(500).send(`Something went wrong while trying to add feedback. Please try again.`)
    }

});

/*
    - This endpoint assumes that the skills will be recieved as a JSON object with an boolean for availability {"availability": true}
    - This endpoint may also never be used if we decide to have a global user update endpoint (its currently in the backlog)
*/
userRouts.post('/:userId/availability', async function(req,res){

    let availability = req.body.availability;

    const dbCollection = getDatabase().collection(usersCollecton);

    let objectId = getObjectId(req.params.userId);
    let user = await dbCollection.findOne({_id: objectId});
    
    if (!user) {
        res.status(404).send("Cannot find user to update availibility.");
        return;
    }

    let result = await dbCollection.updateOne({_id: objectId},{$set: { availibility: availability }});

    if(result.modifiedCount = 1){
        res.status(200).send(`Availibility for ${user.first_name}'s profile updated to ${availability}`);
    }else{
        res.status(404).send(`We could not change availibility to ${availability} for ${user.first_name} Please try again.`);
    }
});

export = userRouts;
