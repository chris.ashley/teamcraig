import React from 'react';
import {Button, OverlayTrigger, Tooltip, Form, Col, Row } from "react-bootstrap";

class SearchBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          skill: ''
        };
        this.handleChange = this.handleChange.bind(this);
      }

    handleChange(event) {
        this.props.onHandleSkillChange(event.target.value);
    }

    render() {
      const renderTooltip = props => (
        <Tooltip {...props}>Try a comma seperated list of skills for better results!</Tooltip>
      );

        return (
          <Row className="justify-content-md-center">
            <Col xs={5}>
              <Form onSubmit={this.props.onHandleSubmitChange}>
                <Form.Group className="mb-3" controlId="searchForm">
                  <OverlayTrigger placement="top" overlay={renderTooltip}>
                    <Form.Control type="text" placeholder="keywords..." value={this.props.skill} onChange={this.handleChange}/>
                  </OverlayTrigger>
                  <br/>
                  
                  <Button type="submit" variant="danger">Submit</Button>      
                </Form.Group>
              </Form>
            </Col>
          </Row> 

        );
      }
};



export default SearchBar;
