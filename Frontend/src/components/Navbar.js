import React from 'react';
import logo from '../image/AND-logo.png'

function Navbar() {
    return (
        <nav className="navbar">
            <div className="navbar-logo-container">
                <a  className="navbar-anchor" href="https://www.and.digital/">
                    <img  className="navbar-logo" src={logo} alt=""/>
                </a>
            </div>
        </nav>
    );
  }

export default Navbar;