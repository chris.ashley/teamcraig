import React from 'react';
import Search from './Search';
import Header from './header';
import profile from '../profile.png';
import { Link } from 'react-router-dom';
import { Row, Col, Badge, ListGroup, Card } from 'react-bootstrap';

class Show extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        skill: '',
        keyword: '',
        result: []
      };
      this.handleSkillChange = this.handleSkillChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
      this.updateKeywords = this.updateKeywords.bind(this);
    }

    handleSkillChange(skill) {
      this.setState({
        skill: skill
        }) 
      }

    handleSubmit(event) {
          event.preventDefault();

          fetch(`/skill/skills/${this.state.skill}`)
          .then((body) => {
            return body.json();
          })
          .then((data) => {
            this.setState({
              result: data,
              showResults: true,
              searchTerm: this.state.skill
            });

            console.log(this.state.skill);
            this.updateKeywords(this.state.skill);
          })
      }

    updateKeywords(searchTerm){
      let keywords = searchTerm.split(",");

      let results = document.querySelector('#keywords-result');
      results.innerHTML = '';

      keywords.forEach(keyword => {
        keyword = keyword.trim();

        let pill = document.createElement('span');
        pill.className = "badge rounded-pill bg-danger keywords-result-pill";
        pill.innerHTML = keyword;

        results.appendChild(pill);
      });

    }

   
    render() {
      return (
        <div className="showpage">     
          <Header title="Search InDemAND"/>   
            <Row>
              <Col xs={3}>
                {/*<h2 className="showpage-title">Search Results</h2>*/}
                <div className="results m-3" style={{display: this.state.showResults ? 'block' : 'none'}}>
                  <h3 id="keywords-title">Keyword(s) : </h3>
                  <h4 id="keywords-result"></h4>
                </div>
              </Col>
              </Row>

              <Row>
              <Col xs={2}></Col>
              <Col xs={8}>
                <Search onHandleSkillChange={this.handleSkillChange} onHandleSubmitChange={this.handleSubmit} skill={this.state.skill}/>
                <ListGroup>
                  {this.state.result.map((user) => (

                    <ListGroup.Item variant='flush' style={{marginBottom: '10px'}}>
                      <Row>
                        <Col xs={4}><Link to={{pathname:"/profile", state:{...user,skill:this.state.skill} }} style={{ color: 'inherit', textDecoration: 'inherit'}}><img src={profile} alt="Profile" className="Profile-logo" /></Link></Col>
                        <Col xs={8}>
                        <div className="user" key="{user.id}">
                        <Link to={{pathname:"/profile", state:{...user,skill:this.state.skill} }} style={{ color: 'inherit', textDecoration: 'inherit'}}><h3 className="showpage-tile-title">{user.first_name} {user.last_name}</h3></Link>
                          <h4>Skills:</h4>
                            <ul className="showpage-tile-skill-list">
                              {user.skills.map((skills) => (
                                <li key="{user}" className="showpage-tile-skill-list-item">{skills}</li>
                              ))}
                            </ul>
                            <div className="showpage-tile-avalibility">
                              <p>The user is {user.availibility ? 'available' : 'not available'}</p>
                            </div>
                            <div className="showpage-tile-rating">
                              <p>rating: {'⭐️'.repeat(user.average_rating)}</p>
                            </div>
                            <a className="contact-btn" href={'mailto:' + user.email + '?subject=Can you help me?&body=Hey ' + user.first_name + ', How are you doing today? I am an ANDi and always looking to improve myself. Thanks to indemAND I have been able to see who knows what within AND. I have noticed that you have an understanding of ' + this.state.skill + '. I am really eager to improve my skills. Would it be ok to connect?'} >
                                      <p className="contact-btn-text">Contact me</p>
                            </a>
                        </div>
                        </Col>
                      </Row>
                    </ListGroup.Item>
                  ))}
                </ListGroup>
                </Col>
                <Col xs={2}></Col>
              </Row>  
            </div>
      );
    }
}

export default Show;