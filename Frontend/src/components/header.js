import {Container, Row, Col, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import AND from "../AND-logo.png";

export default function Header(props){
    return(
        <div id="header">
            <Container>
                <Row>
                <Col xs={2}>
                    <a href="/" ><img src={AND} alt="AND Digital Logo" className="img-fluid"/></a>
                </Col>
                <Col xs={8}></Col>
                <Col xs={2} className="my-auto" ><h3 className="header-text">InDemAND</h3> <Link to='/register'>Register</Link></Col>
                </Row>
            </Container>

            <Row className="indemand-app-header">{props.title}</Row>
        </div>
    )
};