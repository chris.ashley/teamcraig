import { useState } from "react";
import { Button, Row, Container, Alert, Col } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import { useLocation } from "react-router-dom";
import Header from "./components/header";

export default function FeedbackForm() {
  const location = useLocation();
  // States for registering comments
  const [fullName, setFullName] = useState("");
  const [rating, setRating] = useState("");
  const [comments, setComments] = useState("");
  const [anonymize, setAnonymize] = useState("");
  const userID = location.state._id;

  // States for checking the errors
  const [submitted, setSubmitted] = useState(false);
  const [error, setError] = useState(false);

  // Handling the email change
  const handleFullName = (e) => {
    setFullName(e.target.value);
    setSubmitted(false);
  };

  // Handling the rating change
  const handleRating = (e) => {
    setRating(e.target.value);
    setSubmitted(false);
  };

  // Handling the comment change
  const handleComments = (e) => {
    setComments(e.target.value);
    setSubmitted(false);
  };

  // Handling the comment change
  const handleAnonymize = (e) => {
    setAnonymize(e.target.value);
    setSubmitted(false);
  };

  // Handling the feedback form submission
  const handleSubmit = async (e) => {
    e.preventDefault();
    setError(false);
    if (rating === "" || comments === "" || fullName === "") {
      setError(true);
    } else {
      setError(false);
    }
    try {
      const result = await fetch(`/user/${userID}/provideFeedback`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          name: fullName,
          rating: Number.parseInt(rating, 10),
          comment: comments,
          anonymize: anonymize ? true : false,
        }),
      });
      const data = await result.text();
      console.log(data);
      if (result.status === 200) {
        setSubmitted(true);
      }
    } catch (error) {
      console.log(error);
    }
  };

  // Showing success message
  const successMessage = () => {
    return (
      <Row className="justify-content-md-center">
        <Col xs={10}>
          <Alert
            variant="success"
            className=".FeedbackForm-success"
            style={{
              display: submitted ? "" : "none",
            }}
          >
            <h1>Comments Successfully registered!!</h1>
          </Alert>
        </Col>
      </Row>
    );
  };

  // Showing error message if a field is blank
  const errorMessage = () => {
    return (
      <Row className="justify-content-md-center">
        <Col xs={10}>
          <Alert
            variant="danger"
            className=".FeedbackForm-error"
            style={{
              display: error ? "" : "none",
            }}
          >
            <h1>Please enter all the fields!</h1>
          </Alert>
        </Col>
      </Row>
    );
  };

  return (
    <div>
      <Header title="InDemAND Feedback Form"/>

      {/* Calling to the methods */}
      <div className=".FeedbackForm-messages">
        {errorMessage()}
        {successMessage()}
      </div>

      <Row className="justify-content-md-center">
        <Col xs={3}></Col>
        <Col xs={6}>
          <Container className=".FeedbackForm-body">
            <Form>
              {/* Labels and inputs for form data */}

              <Form.Group
                className=".FeedbackForm-mb-3"
                controlId="formEmail"
                onChange={handleFullName}
                value={fullName}
              >
                <Form.Label>Full Name</Form.Label>
                <Form.Control type="text" placeholder="Enter Full Name" />
              </Form.Group>

              <Form.Group
                className=".FeedbackForm-mb-3"
                controlId="formComments"
                onChange={handleComments}
                value={comments}
              >
                <Form.Label>Comments</Form.Label>
                <Form.Control type="text" placeholder="Enter comments" />
              </Form.Group>

              <Form.Group
                className=".FeedbackForm-mb-3"
                controlId="formRating"
                onChange={handleRating}
                value={rating}
              >
                <Form.Label>Rating</Form.Label>
                <Form.Control type="integer" placeholder="Choose rating" />
              </Form.Group>

              <Form.Group inline
                className=".FeedbackForm-mb-3"
                controlId="formAnonymize"
                onChange={handleAnonymize}
                value={anonymize}
              >
                <Form.Check type="checkbox" label="Anonymize?"/>
              </Form.Group>
            </Form>

            <Button
              onClick={handleSubmit}
              type="submit"
              variant="danger"
              className=".FeedbackForm-mentorBut mtop3"
            >
              <b>Submit</b>
            </Button>
          </Container>
        </Col>
        <Col xs={3}></Col>
      </Row>
    </div>
  );
}
