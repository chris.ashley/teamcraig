import Profile from "./Profile";
import RegForm from "./RegForm";
import Show from "./components/Show";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import FeedbackForm from "./FeedbackForm";

import "bootstrap/dist/css/bootstrap.css";
import "./main.css";

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/profile">
          <Profile />
        </Route>
        <Route path="/register">
          <RegForm/>
        </Route>
        <Route path="/feedback">
          <FeedbackForm />
        </Route>
        <Route path="/">
          <div className="App">
            <Show />
          </div>
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
