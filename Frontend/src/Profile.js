import React from 'react';
import Header from './components/header';
import profile from './profile.png';
import { Button, Row, Col, Container, Card, Toast } from 'react-bootstrap';
import { useLocation, Link } from "react-router-dom";


const Profile = (props) => {
  const location = useLocation();
  const data = location.state;
  
  return <div>
    <Header title="InDemAND Profile"/>

    <Container className="body">
      <Row>
        <Col xs={4}></Col>
        <Col xs={4}>
          <img src={profile} alt="Profile" className="Profile-logo" />
        </Col>
        <Col xs={4}></Col>
      </Row>

      <Row>
        <Col xs={4}></Col>
        <Col xs={6}>
          <div style={{marginTop: "1rem"}}>
            <h3>Name: {data.first_name + ' ' + data.last_name}</h3>
            <h3>Email: {data.email}</h3>
            <h3>Position: Developer </h3>
            <h3>Skills: {data.skills.map((skill) => (
                          skill
                        )).join(', ') }</h3>
            
            <br/>
          </div>
        </Col>
        <Col xs={4}></Col>
      </Row>
      <Row>
        <Button variant="danger" href={'mailto:' + data.email + '?subject=Can you help me?&body=Hey ' + data.first_name + ', How are you doing today? I am an ANDi and always looking to improve myself. Thanks to indemAND I have been able to see who knows what within AND. I have noticed that you have an understanding of ' + data.skill + '. I am really eager to improve my skills. Would it be ok to connect?'}>Contact Me</Button>
      
          <Link to={{ pathname: "/feedback", state: data }} className="btn">
            <Button>Provide Feedback</Button>
          </Link>

      </Row>
    </Container>

    <Container>

    <h1>Feedback {data.feedback.length > 0 ? '(' + data.feedback.length + ')' : ''}:</h1>

    {data.feedback.map((feedback) => (
          <Row>
            <Col xs={1}></Col>
            <Col>
            <Toast className="m-3">
              <Toast.Header closeButton={false}>
                <strong className="me-auto">{feedback.name}</strong>
                <small>{'⭐️'.repeat(feedback.rating)}</small>
              </Toast.Header>
              <Toast.Body>{feedback.comment}</Toast.Body>
            </Toast>
            </Col>
          </Row>
        ))}

    </Container>


  </div>;
};
export default Profile;