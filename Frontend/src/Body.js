import {
    BrowserRouter as Router,
    Switch, 
    Route,
} from "react-router-dom";
import Profile from "./Profile";

const Body = ( ) =>{
    return (
        <div>
            <Switch>
                <Route path="/profile">
                    <Profile />
                </Route>
            </Switch>
        </div>
    )
}


export default Body