import { useState, useEffect } from "react";
import { Button, Row, Container, Alert, Col, InputGroup } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import Header from "./components/header";
import { FaMinusCircle } from "react-icons/fa"

export default function RegForm() {
  // States for registration
  const [first_name, setFirstName] = useState("");
  const [last_name, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [available, setAvailable] = useState("");
  const [skillSelected, setSkillSelected] = useState("");
  const [skills, setSkills] = useState([]);
  const [skillList, setSkillList] = useState([])

  // States for checking the errors
  const [submitted, setSubmitted] = useState(false);
  const [error, setError] = useState(false);
  const [duplicateError, setDuplicateError] = useState(false);
  const [formatError, setFormatError] = useState(false);

  useEffect(async () => {
    const response = await fetch('/skill/getAllSkills')
    const body = await response.json();
    setSkillList(body)
  }, []);

  // Handling the first name change
  const handleFirstName = (e) => {
    setFirstName(e.target.value);
    setSubmitted(false);
  };

  // Handling the last name change
  const handleLastName = (e) => {
    setLastName(e.target.value);
    setSubmitted(false);
  };

  // Handling the email change
  const handleEmail = (e) => {
    setEmail(e.target.value);
    setSubmitted(false);
  };

  const handleAvailable = (e) => {
    setAvailable(e.target.value);
    setSubmitted(false);
  };

  const handleSkillSelect = (e) => {
    setSkillSelected(e.target.value);
    setSubmitted(false);
  };

  const addSkill = (e) => {
    const newSkills = [...skills]
    newSkills.push(skillSelected)
    setSkills(newSkills)
    setSubmitted(false);
  };

  const deleteSkill = (e) => {
    const newSkills = [...skills]
    newSkills.splice(e.target.value, 1);
    setSkills(newSkills)
    setSubmitted(false);
  }

  // Handling the form submission
  const handleSubmit = async (e) => {
    e.preventDefault();
    setDuplicateError(false);
    setError(false);
    setFormatError(false);
    if (first_name === "" || last_name === "" || email === "") {
      setError(true);
    } else {
      setError(false);
    }
    try {
      const result = await fetch("/register", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          first_name,
          last_name,
          email,
          available: available ? true : false,
          skills
        }),
      });
      const data = await result.text();
      console.log(data);
      if (data === "User with that e-mail address already exists") {
        setDuplicateError(true);
      }
      if (data === "User must have an AND.Digital email address") {
        setFormatError(true);
      }
      if (data === "User created") {
        setSubmitted(true);
      }
    } catch (error) {
      console.log(error);
    }
  };

  // Showing success message
  const successMessage = () => {
    return (
      <Row className="justify-content-md-center">
        <Col xs={10}>
          <Alert
            variant="success"
            className="RegForm-success"
            style={{
              display: submitted ? "" : "none",
            }}
          >
            <h1>User {first_name + " " + last_name} Successfully registered!!</h1>
          </Alert>
        </Col>
      </Row>
    );
  };

  // Showing error message if a field is blank
  const errorMessage = () => {
    return (
      <Row className="justify-content-md-center">
        <Col xs={10}>

          <Alert
            variant="danger"
            className="RegForm-error"
            style={{
              display: error ? "" : "none",
            }}
          >
            <h1>Please enter all the fields!</h1>
          </Alert>
        </Col>
      </Row>

    );
  };

  //Showing error message if email already exists on system
  const duplicateEmailErrorMessage = () => {
    return (
      <Row className="justify-content-md-center">
        <Col xs={10}>
          <Alert
            variant="danger"
            className="RegForm-error"
            style={{
              display: duplicateError ? "" : "none",
            }}
          >
            <h1>Email address already registered!</h1>
          </Alert>
        </Col>
      </Row>
    );
  };

  const incorrectEmailFormatMessage = () => {
    return (
      <Row className="justify-content-md-center">
        <Col xs={10}>
          <Alert
            variant="danger"
            className="RegForm-error"
            style={{
              display: formatError ? "" : "none",
            }}
          >
            <h1>User must have an AND.Digital email address!</h1>
          </Alert>
        </Col>
      </Row>
    );
  };

  return (
    <div>
      <Header title="InDemAND Registration Form" />

      {/* Calling to the methods */}
      <div className="RegForm-messages">
        {errorMessage()}
        {successMessage()}
        {duplicateEmailErrorMessage()}
        {incorrectEmailFormatMessage()}
      </div>

      <Row className="justify-content-md-center">
        <Col xs={3}></Col>
        <Col xs={6}>
          <Container>
            <Form>
              {/* Labels and inputs for form data */}
              <Form.Group
                className="RegForm-mb-3"
                controlId="formFirstName"
                onChange={handleFirstName}
                value={first_name}
              >
                <Form.Label>First Name</Form.Label>
                <Form.Control type="text" placeholder="First Name" />
              </Form.Group>

              <Form.Group
                className="RegForm-mb-3"
                controlId="formLastName"
                onChange={handleLastName}
                value={last_name}
              >
                <Form.Label>Last Name</Form.Label>
                <Form.Control type="text" placeholder="Last Name" />
              </Form.Group>

              <Form.Group
                className="RegForm-mb-3"
                controlId="formBasicEmail"
                onChange={handleEmail}
                value={email}
              >
                <Form.Label>Email address</Form.Label>
                <Form.Control type="email" placeholder="Email Address" />
                <Form.Text className="RegForm-text-muted">
                  This must be an AND Digital email address.
                </Form.Text>
              </Form.Group>

              <Form.Group
                className=".RegForm-mb-3"
                controlId="formAvailabile"
                onChange={handleAvailable}
                value={available}
              >
                <Form.Check type="checkbox" label="Available?" />
              </Form.Group>

              <Form.Group
                className=".RegForm-mb-3"
                controlId="formSkillSelect"
                onChange={handleSkillSelect}
                value={skillSelected}
              >
                <InputGroup className=".RegForm-mb-3">
                  <Form.Select label="Skills">
                    <option key='blankChoice' hidden value />
                    {skillList.map((skill) =>  <option key={skill.name} value={skill.name}>{skill.name}</option>)}
                  </Form.Select>
                  <Button variant="primary" id="addSkillButton" disabled={!skillSelected} onClick={addSkill}>Add</Button>
                </InputGroup>
              </Form.Group>

              

              <Form.Group
                className=".RegForm-mb-3"
                controlId="formSkillsList"
              >
                <div className=".RegForm-mb-3">
              {skills.map((skill, index) =>  
                <Button className="RegForm-skill-button" variant="danger" id={`skill${index}`} key={`skill${index}`} value={index} onClick={deleteSkill}><FaMinusCircle/> {skill}</Button>
              )}
              </div>
              </Form.Group>
            </Form>

            <Button
              onClick={handleSubmit}
              type="submit"
              variant="primary"
            >
              <b>Submit</b>
            </Button>

          </Container>
        </Col>
        <Col xs={3}></Col>
      </Row>
    </div>
  );
}
